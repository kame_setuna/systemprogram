#include "Header.h"

typedef struct stack {
	char data[BUFSIZ];
	char id;
}S;

void pushc(char s, S *x) {
	x->data[x->id] = s;
	(x->id)++;
}

char popc(S *x) {
	(x->id)--;
	return x->data[x->id];
}

char *intopost(char *s) {
	int i,j;
	S stack;
	stack.id = 0;
	for(i = 0, j = 0; s[i] != '\0'; i++) {
		switch(s[i]) {
			case '+':
				while(stack.id != 0) {
					s[j] = popc(&stack);
					j++;
				}
				pushc('+', &stack);
				break;
			case '-':
				while(stack.id != 0) {
					if(stack.data[stack.id] == '-' ||
						stack.data[stack.id] == '*' ||
						stack.data[stack.id] == '/')
						s[j] = popc(&stack), j++;
					else
						break;
				}
				pushc('-', &stack);
				break;
			case '*':
				while(stack.id != 0) {
					if(stack.data[stack.id] == '*' ||
						stack.data[stack.id] == '/')
						s[j] = popc(&stack), j++;
					else
						break;
				}
				pushc('*', &stack);
				break;
			case '/':
				while(stack.id != 0) {
					if(stack.data[stack.id] == '/')
						s[j] = popc(&stack), j++;
					else
						break;
				}
				pushc('/', &stack);
				break;
			default:
				s[j] = s[i];
				j++;
				break;
		}
	}
	
	while(stack.id != 0) {
		s[j] = popc(&stack);
		j++;
	}
	return s;
}