#define _CRT_SECURE_NO_WARNINGS

#include "Header.h"

typedef struct stack {
	Node *data[BUFSIZ];
	char id;
}S;

void push(Node *s, S *x) {
	x->data[x->id] = s;
	(x->id)++;
}

Node *pop(S *x) {
	(x->id)--;
	return x->data[x->id];
}

void preorder(Node *p) {
	if(p == NULL) return;
	putchar(p->data);
	preorder(p->left);
	preorder(p->right);
	return;
}

void postorder(Node *p) {
	if(p == NULL) return;
	postorder(p->left);
	postorder(p->right);
	putchar(p->data);
	return;
}

int main(void) {
	S stack;
	stack.id = 0;
	char s[BUFSIZ];
	Node *root;
	int i = 0;

	scanf("%s", s);
	intopost(s);

	for(i = 0; s[i] != '\0'; i++) {
		root = malloc(sizeof(Node));
		root->data = s[i];
		root->left = root->right = NULL;
		if(isdigit(s[i]))
			push(root, &stack);
		else {
			root->right = pop(&stack);
			root->left = pop(&stack);
			push(root, &stack);
		}
	}

	preorder((stack.data)[0]);
	putchar('\n');
	postorder((stack.data)[0]);
	putchar('\n');

	return 0;
}