#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

typedef struct node {
	char data;
	struct node *right;
	struct node *left;
}Node;

char *intopost(char * s);