#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

void error(void) {
	puts("浮動小数点定数ではありません");
	exit(1);
}

double myPow(int X, int Y,char sign) {
	double ans;
	if(sign == '+')
		ans = pow(X, Y);
	else if(sign == '-')
		ans = pow(X, -Y);
	return ans;
}

int main(void) {
	char src[BUFSIZ], sign;
	int i = 0;

	scanf("%s", src);

	if(src[i] == '.') {
		i++;
		if(!isdigit(src[i])) error();
	} else if(isdigit(src[i])) {
		do {
			i++;
		} while(isdigit(src[i]));
		if(src[i] == 'E') goto exp;
		if(src[i] != '.') error();
	} else
		error();
	i++;
	while(isdigit(src[i])) {
		i++;
	}
	if(src[i] != 'E') goto calc;
exp:
	sign = '+', i++;
	if(src[i] == '+' || src[i] == '-') {
		sign = src[i], i++;
	}
	while(isdigit(src[i])) {
		i++;
	}
calc:
	if(src[i] != '\0')
		error();
	else
		puts("浮動小数点定数です");
	return 0;
}