#include "Header.h"

typedef struct node {
	int dnm;
	int num;
}Frt;

int euclid(int a, int b) {
	int temp;

	if(a < b) { temp = a; a = b; b = temp; }
	if(b < 1) return -1;

	if(a % b == 0) return b;
	return euclid(b, a % b);
}

Frt add(Frt a, Frt b) {
	Frt answer;
	int temp;

	answer.dnm = a.dnm * b.dnm;
	answer.num = a.num * b.dnm + b.num * a.dnm;
	temp = euclid(answer.dnm, answer.num);
	answer.dnm /= temp;
	answer.num /= temp;
	return answer;
}

Frt sub(Frt a, Frt b) {
	Frt answer;
	int temp;

	answer.dnm = a.dnm * b.dnm;
	answer.num = a.num * b.dnm - b.num * a.dnm;
	temp = euclid(answer.dnm, answer.num);
	answer.dnm /= temp;
	answer.num /= temp;
	return answer;
}

Frt mlt(Frt a, Frt b) {
	Frt answer;
	int temp;

	answer.dnm = a.dnm * b.dnm;
	answer.num = a.num * b.num;
	temp = euclid(answer.dnm, answer.num);
	answer.dnm /= temp;
	answer.num /= temp;
	return answer;
}

Frt division(Frt a, Frt b) {
	Frt answer;
	int temp;

	answer.dnm = a.dnm * b.num;
	answer.num = a.num * b.dnm;
	temp = euclid(answer.dnm, answer.num);
	answer.dnm /= temp;
	answer.num /= temp;
	return answer;
}