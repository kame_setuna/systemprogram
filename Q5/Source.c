#define _CRT_SECURE_NO_WARNINGS
#include "Header.h"

/* 再帰で順列を生成，n が要素数，c が着目している要素番号 */
void genPerm(int p[], int n, int c) {
	int i, tmp;

	if(c == n) { /* すべて埋まったら出力して再帰停止 */
		for(i = 1; i <= n; i++) {
			printf("%d ", p[i]);
		}
		putchar('\n');
		return;
	}
	for(i = c; i <= n; i++) {
		/* p[i] を p[c] の位置に移す（交換で） */
		tmp = p[i]; p[i] = p[c]; p[c] = tmp;
		/* p[c] が仮定されるので，p[c + 1] 以降を決めるべく再帰 */
		genPerm(p, n, c + 1);
	}
	/* もとの並びに戻す */
	tmp = p[c];
	for(i = c; i < n; i++) {
		p[i] = p[i + 1];
	}
	p[n] = tmp;

	return;
}

/* エントリポイント */
int main(void) {
	int p[5], n, i;

	p[0] = 0;
	p[1] = 3;
	p[2] = 5;
	p[3] = 8;
	p[4] = 9;
	n = 4;

	genPerm(p, n, 1);

	return 0;
}