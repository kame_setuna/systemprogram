#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

typedef struct stack {
	char data[BUFSIZ];
	char d;
}S;

void push(char s, S *x) {
	x->data[x->d] = s;
	(x->d)++;
}

char pop(S *x) {
	(x->d)--;
	return x->data[x->d];
}

int main(void) {
	char s[BUFSIZ];
	int i = 0;
	S stack;
	stack.d = 0;

	scanf("%s", s);

	for(i = 0; s[i] != '\0'; i++) {
		switch(s[i]) {
			case '+':
				while(stack.d != 0) {
					putchar(pop(&stack));
				}
				push('+', &stack);
				break;
			case '-':
				while(stack.d != 0) {
					if(stack.data[stack.d] == '-' ||
						stack.data[stack.d] == '*' ||
						stack.data[stack.d] == '/')
						putchar(pop(&stack));
					else
						break;
				}
				push('-', &stack);
				break;
			case '*':
				while(stack.d != 0) {
					if(stack.data[stack.d] == '*' ||
						stack.data[stack.d] == '/')
						putchar(pop(&stack));
					else
						break;
				}
				push('*', &stack);
				break;
			case '/':
				while(stack.d != 0) {
					if(stack.data[stack.d] == '/')
						putchar(pop(&stack));
					else
						break;
				}
				push('/', &stack);
				break;
			default:
				putchar(s[i]);
				break;
		}
	}

	while(stack.d != 0) {
		putchar(pop(&stack));
	}
	putchar('\n');
	return 0;
}