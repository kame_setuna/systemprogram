#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

typedef struct stack {
	int data[BUFSIZ];
	int d;
}S;

void push(int s, S *x){
	x->data[x->d] = s;
	(x->d)++;
}
 
int pop(S *x){
	(x->d)--;
	return x->data[x->d];
}

int main(void){
	int i = 0;
	char s[BUFSIZ];
	S t;
	t.d = 0;

	scanf("%s",s);

	while(s[i] != '\0'){
		if(isdigit((int)s[i]))
			push(s[i] - '0', &t);
		else
			switch(s[i]){
			case '*':
				push(pop(&t)*pop(&t),&t);
				break;
			case '/':
				push(pop(&t)/pop(&t),&t);
				break;
			case '+':
				push(pop(&t)+pop(&t),&t);
				break;
			case '-':
				push(pop(&t)-pop(&t),&t);
				break;
			default:
				break;
		}
		i++;
	}
	printf("%d\n",t.data[0]);
	return 0;
}